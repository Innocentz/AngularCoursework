import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';

@Component({
  selector: 'app-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.css']
})
export class CockpitComponent implements OnInit {
 @Output() serverCreated = new EventEmitter<{serverName: string, serverContent: string}>(); // emit event to notify the parent component
 @Output() bluePrintCreated = new EventEmitter<{serverName: string, serverContent: string}>(); // @Output make the event listenable
 @ViewChild('serverContentInput') serverContentInput;
 newServerContent = '';
  newServerName = '';
  constructor() { }

  ngOnInit() {
  }

  onAddServer() {
    console.log(this.serverContentInput.nativeElement.value);
   this.serverCreated.emit(
     {
       serverName: this.newServerName,
       serverContent: this.serverContentInput.nativeElement.value
     });
  }

  onAddBlueprint() {
   this.bluePrintCreated.emit(
    {
      serverName: this.newServerName,
      serverContent: this.newServerContent
    });
  }
}
